package com.example.demo.service;

import com.example.demo.bean.Requests;
import com.example.demo.bean.Statistic;
import com.example.demo.dao.NginxDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class NginxService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NginxService.class);
    final static String COMMAND = "curl http://localhost/server-status";
    final static String ACTIVE_CONNECTIONS_REGEX = "/(\\d+)/";//first number
    final static String ACCEPTS_REGEX = "[^\\d]*[\\d]+[^\\d]+([\\d]+)";//second number
    final static String HANDLED_REGEX = "[^\\d]*[\\d]+[^\\d]*[\\d]+[^\\d]+([\\d]+)";//third number
    final static String REQUESTS_REGEX = "[^\\d]*[\\d]+[^\\d]*[\\d]+[^\\d]*[\\d]+[^\\d]+([\\d]+)";//fourth number
    final static Pattern ACTIVE_CONNECTIONS_PATTERN = Pattern.compile(ACTIVE_CONNECTIONS_REGEX);
    final static Pattern ACCEPTS_PATTERN = Pattern.compile(ACCEPTS_REGEX);
    final static Pattern HANDLED_PATTERN = Pattern.compile(HANDLED_REGEX);
    final static Pattern REQUESTS_PATTERN = Pattern.compile(REQUESTS_REGEX);
    static String status = getStringByServerStatus();

    @Autowired
    private NginxDAO nginxDAO;

    public Statistic getAll() throws IOException {
        return parseResponse();
    }

    public static String getStringByServerStatus() {
        Process process = null;
        try {
            process = Runtime.getRuntime().exec(COMMAND);
        } catch (IOException e) {
            LOGGER.info("Exception in getStringByServerStatus()");
        }
        InputStream inputStream = process.getInputStream();
        return new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining("\n"));
    }

    public static Statistic parseResponse() throws IOException {
        Requests requests = new Requests();
        Statistic statistic = new Statistic();
        requests.setAccepts(String.valueOf(getDataByServerStatus(ACCEPTS_PATTERN)));
        requests.setHandled(String.valueOf(getDataByServerStatus(HANDLED_PATTERN)));
        requests.setRequests(String.valueOf(getDataByServerStatus(REQUESTS_PATTERN)));
        statistic.setActiveConnections(String.valueOf(getDataByServerStatus(ACTIVE_CONNECTIONS_PATTERN)));
        statistic.setRequests(requests);
        return statistic;
    }

    public static String getDataByServerStatus(Pattern pattern) throws IOException {
        Matcher matcher = pattern.matcher(status);
        if (matcher.find()) {
            return matcher.group(1);
        } else {
            return null;
        }
    }
}
