package com.example.demo.controller;

import com.example.demo.bean.Statistic;
import com.example.demo.service.NginxService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;

@RestController
@RequestMapping("/nginx")
public class NginxController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NginxController.class);

    @Autowired
    private final NginxService nginxService;

    public NginxController(NginxService nginxService) {
        this.nginxService = nginxService;
    }

    @GetMapping("/statistics")
    public @ResponseBody
    Statistic getAll() throws IOException {
        return nginxService.getAll();
    }



}

