package com.example.demo.exception;

public class NginxException extends RuntimeException  {
    public NginxException() {
        super();
    }

    public NginxException(String message) {
        super(message);
    }

    public NginxException(String message, Throwable cause) {
        super(message, cause);
    }

    public NginxException(Throwable cause) {
        super(cause);
    }

    protected NginxException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
