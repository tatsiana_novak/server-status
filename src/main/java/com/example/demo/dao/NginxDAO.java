package com.example.demo.dao;

import com.example.demo.bean.Statistic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface NginxDAO extends CrudRepository<Statistic, Long> {
   }
