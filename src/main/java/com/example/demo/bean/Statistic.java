package com.example.demo.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;


@Entity
@Table(name = "statistic")
public class Statistic {
    @Id
    @Column(name = "idstatistic", nullable = false, unique = true)
    @GeneratedValue(strategy = GenerationType.AUTO)
    @JsonIgnore
    private long id;

    @Column(name = "activeConnections", nullable = false)
    private String activeConnections;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "requests_idrequests")
    private Requests requests;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getActiveConnections() {
        return activeConnections;
    }

    public void setActiveConnections(String activeConnections) {
        this.activeConnections = activeConnections;
    }

    public Requests getRequests() {
        return requests;
    }

    public void setRequests(Requests requests) {
        this.requests = requests;
    }
}

