-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema dbase
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `dbase` ;

-- -----------------------------------------------------
-- Schema dbase
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `dbase` DEFAULT CHARACTER SET utf8 ;
USE `dbase` ;

-- -----------------------------------------------------
-- Table `dbase`.`requests`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbase`.`requests` ;

CREATE TABLE IF NOT EXISTS `dbase`.`requests` (
  `idrequests` INT NOT NULL AUTO_INCREMENT,
  `accepts` VARCHAR(45) NOT NULL,
  `handled` VARCHAR(45) NOT NULL,
  `requests` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idrequests`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `dbase`.`statistic`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `dbase`.`statistic` ;

CREATE TABLE IF NOT EXISTS `dbase`.`statistic` (
  `idstatistic` INT NOT NULL AUTO_INCREMENT,
  `activeConnections` VARCHAR(45) NOT NULL,
  `requests_idrequests` INT NOT NULL,
  PRIMARY KEY (`idstatistic`),
  CONSTRAINT `fk_statistic_requests`
    FOREIGN KEY (`requests_idrequests`)
    REFERENCES `dbase`.`requests` (`idrequests`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE INDEX `fk_statistic_requests_idx` ON `dbase`.`statistic` (`requests_idrequests` ASC) VISIBLE;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
