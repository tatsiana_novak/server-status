package com.example.demo.service;

import com.example.demo.bean.Statistic;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.io.IOException;

class NginxServiceTest {

    @Test
    void parseResponse() throws IOException {
        String response = "Active connections: 1 \n" +
                "server accepts handled requests\n" +
                " 28 28 28 \n" +
                "Reading: 0 Writing: 1 Waiting: 0 ";
       Statistic statistic = NginxService.parseResponse();
        Assert.assertEquals("1" , statistic.getActiveConnections());
        Assert.assertEquals("2" , statistic.getActiveConnections());
        Assert.assertEquals("3" , statistic.getActiveConnections());
    }
}